//var auth = require('./middleware/autendicador-falho'); //FIXME não consegui elaborar um global :(
var path = require('path');
var express = require('express');
var cookie = require('cookie');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session'); //TODO Não entendi direito a vantagem desse middleware e nem o objetivo dele
var expressSession = require('express-session');
var load = require('express-load');
var bodyParser = require('body-parser')
var methodOverride = require('method-override');
var passport = require('passport');
var LocalStrategy = require('passport-local');
var passportIo = require('passport.socketio');

const KEY = 'ntalk.sid';
const SECRET = 'ntalk.s3cr3t';
const APP_NAME = 'ntalk';
const MEMORY_STORE = new expressSession.MemoryStore();

var error = require('./middleware/error');

//Middlewares das rotas do sistema. Não está sendo utilizado por que estamos usanod o express-load
//var indexRoute = require('./routes-sem-express-load');
//var usuarioRoute = require('./routes-sem-express-load/users.js');

var app = express();


//socket.io
//var http = require('http');
//var server = http.createServer(app);
//var io = require('socket.io').listen(server);
//var io = require('socket.io')(80);


// configurando view engine do express(ejs)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//Habilitando o cookieSession na stack de configuração. Esse middleware (módulo cookie-session) habilita a criação de sessão via cookies.
//var store = new require('express-session').MemoryStore(); //Não funcionou :(
cookieSession = cookieSession({
//    keys: ['secret1', 'secret2']
    name: APP_NAME,
    secret: SECRET
//    ,store: store
});
//app.use(cookieSession);
app.use(cookieParser(SECRET));
app.use(expressSession({
        key:    KEY,
        store:  MEMORY_STORE,
        secret: SECRET
    }
));

//Body parse: faz a conversão dos inputs da requisição para objeto json
app.use(bodyParser.urlencoded());

//Permite usar o mesmo path entre métodos HTTP diferentes
app.use(methodOverride());

app.use(express.static(path.join(__dirname, 'public')));

//Rotas
// ---- ROTAS SEM UTILIZAR O EXPRESS-LOAD -----
//Routes (se não informar o path, é considerado "/". Estou (use)ando as rotas como middlewares ("express.Router()"), expostos no exports do diretório "/routes"
//app.use(indexRoute);
//app.use("/usuario", usuarioRoute);

//app.use(auth); //FIXME não consegui elaborar um global :(


// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(obj, done) {
    done(null, obj);
});



app.use(passport.initialize());
app.use(passport.session());

//Rotas usando o express-load (facilitador para utilização das rotas)
load('models')
    .then('controllers')
    .then('routes')
    .into(app);


passport.use("*", new LocalStrategy.Strategy({passReqToCallback: true},
    function(req, email, password, done) {
        console.log(req);
        var sessionID = req.signedCookies[KEY];
//        store.get(sessionID, function(err, session) {
//            if (err || !session) {
//                done(null, false, { message: 'Ainda não possui uma sessão!' })
//            } else {
//                var usuario = req.body.usuario;
//                if (!usuario.email) {
//                    done(null, false, { message: 'Informe o email!' })
//                }
//                req.session.usuario = usuario;
//                done(null, usuario);
//            }
//        });
    }
));

//Colocado depois de todos os middlewares. Se não fizer o match de nenhuma URL ou se houver algum erro (parâmetro err no serverError()), utilizamos esse middleware.
app.use(error.notFound);
app.use(emrror.serverError);


var server = app.listen(3000, function(){
    console.log("Ntalk no ar.");
});

var io = require('socket.io').listen(server);

io.on('connection', function(socket){
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
    });
});

io.use(function(socket, next) {
    var handshakeData = socket.request;
    var c = cookie.parse(socket.request.headers.cookie);
//    console.log(store);
    console.log(c);
//    log.console(cookieParser.signedCookie(handshakeData.headers.cookie, SECRET));
    // make sure the handshake data looks good as before
    // if error do this:
    // next(new Error('not authorized');
    // else just call next
//    next();
});

io.use(passportIo.authorize({
    cookieParser: cookieParser,
    key:          SECRET,       // the name of the cookie where express/connect stores its session_id
    secret:       APP_NAME,    // the session_secret to parse the cookie
    store:        MEMORY_STORE // we NEED to use a sessionstore. no memorystore please    >:D
    ,success:     onAuthorizeSuccess  // *optional* callback on success - read more below
//    fail:        onAuthorizeFail,     // *optional* callback on fail/error - read more below
}));

function onAuthorizeSuccess(data, accept) {
    console.log('successful connection to socket.io');
    accept(null, true);
}

load('sockets')
    .into(io);

