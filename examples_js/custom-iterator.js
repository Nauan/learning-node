/**
 * Created by HOME on 23/06/2014.
 */

function Range(low, high){
    this.low = low;
    this.high = high;
}
Range.prototype.__iterator__ = function(){
    for (var i = this.low; i <= this.high; i++)
          continue;
//        yield i; TODO Somente com o ES6 :(
};
var range = new Range(3, 5);
for (var i in range)
    print(i); // prints 3, then 4, then 5 in sequence