/**
 * Created by HOME on 15/06/2014.
 */

module.exports = function(app) {

    var autenciar = require('./../middleware/passport-auth'); //FIXME Não gostei disso. Tem que adicionar como call back em tudo :(
    var contatos  = app.controllers.ContatoController;

    app.get('/contatos', autenciar, contatos.index);
    app.get('/contato/:id', autenciar, contatos.show);
    app.post('/contato', autenciar, contatos.create);
    app.get('/contato/:id/editar', autenciar, contatos.edit);
    app.put('/contato/:id', autenciar, contatos.update);
    app.delete('/contato/:id', autenciar, contatos.destroy);
};