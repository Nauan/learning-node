module.exports = function(app) {
    var autenciar = require('./../middleware/passport-auth'); //FIXME Não gostei disso. Tem que adicionar como call back em tudo :(
    var chat = app.controllers.ChatController;
    app.get('/chat/:email', autenciar, chat.index);
};