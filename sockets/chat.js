module.exports = function(io) {
    var crypto = require('crypto');
    var sockets = io.sockets;
    sockets.on('connection', function (client) {

        if (client.request.user.contatos) {
            client.request.user.contatos.forEach(function(contato) {
                client.emit('notify-offline', contato.email);
            });
        }


        //Aviso todos os clientes que fiquei online. Também aviso a mim mesmo sobre os outros clientes que já estão online ;)
        for (c in sockets.connected) {
            var email = sockets.connected[c].client.request.user.email;
            client.emit('notify-onlines', email);
            client.broadcast.emit('notify-onlines', email);
        }

        console.log(sockets.connected.length);
        console.log(io.of('/'));
        client.on('join', function(sala) {
           if (sala) {
               sala = sala.replace('?', '');
           } else {
               var timestamp = new Date().toString();
               var md5 = crypto.createHash('md5');
               sala = md5.update(timestamp).digest('hex');
           }
            client.sala = sala; //FIXME não fazer isso (não armazenar a sala em memória, mas sim em um objeto que faça sentido (redis por ex))!
            client.join(sala);
        });

        //Ao descontectar, agora vou mostrar uma mensagem e avisar a todos que esse email ficou offline
        client.on('disconnect', function () {
            var msg = "<b>"+ client.request.user.nome +":</b> saiu.<br>";
            client.broadcast.emit('notify-offline', client.request.user.email);
            sockets.in(client.sala).emit('send-client', msg);
            client.leave(client.sala); //FIXME não fazer isso (não armazenar a sala em memória, mas sim em um objeto que faça sentido (redis por ex))!
        });

        client.on('send-server', function (data) {
            var usuario = client.request.user;
            var msg = "<b>"+ usuario.nome+":</b> "+data+"<br>";
            var data = {email: usuario.email, sala: client.sala};
            client.broadcast.emit('new-message', data);
            sockets.in(client.sala).emit('send-client', msg);
        });
    });
}