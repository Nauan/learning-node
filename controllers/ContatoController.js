/**
 * Created by HOME on 15/06/2014.
 */

module.exports = function(app) {
    var ContatoController = {
        index: function(req, res) {
            app.models.usuario.findById(req.user._id, function(err, usuario) {
                res.render('contatos/index', {usuario: usuario});
            });
        },
        create: function(req, res) {
            app.models.usuario.findById(req.user._id, function(erro, usuario) {
                usuario.contatos.push(req.body.contato);
                usuario.save();
                res.redirect('/contatos'); //Redireciona o usuário para outra URL (a requisição é feita novamente, muda a URL)
            });
//            app.models.usuario.update({email: usuario.email}, usuario).exec(); //Atualiza sem ter que buscar antes

        },
        show: function(req, res) {
            var id = req.params.id;
            app.models.usuario.findById(req.user._id, function(erro, usuario) {
                var contato = usuario.contatos.id(id);
                var params = {contato: contato, id: id};
                res.render('contatos/show', params); //Apenas renderiza a tela, a URL permanece a mesma e nenhuma outra requisição é feita.
            });
        },
        edit: function(req, res) {
            var id = req.params.id;
            app.models.usuario.findById(req.user._id, function(erro, usuario) {
                var contato = usuario.contatos.id(id);
                var params = {contato: contato, id: id};
                res.render('contatos/edit', params); //Apenas renderiza a tela, a URL permanece a mesma e nenhuma outra requisição é feita.
            });

        },
        update: function(req, res) {
            app.models.usuario.findById(req.user._id, function(erro, usuario) {
                var contato = usuario.contatos.id(req.params.id);
                contato.nome = req.body.contato.nome;
                contato.email = req.body.contato.email;
                usuario.save(function() {
                    res.redirect('/contatos');
                });
            });

        },
        destroy: function(req, res) {
            app.models.usuario.findById(req.user._id, function(erro, usuario) {
                var contato = usuario.contatos.id(req.params.id);
                contato.remove();
                usuario.save(function() {
                    res.redirect('/contatos');
                });
            });
        }
    }
    return ContatoController;
};