/**
 * Created by HOME on 15/06/2014.
 */

module.exports = function(app) {
    var ChatController = {
        index: function(req, res) {
            var resultado = {email: req.params.email, usuario: req.user};
            res.render('chat/index', resultado);
        }
    };
    return ChatController;
};