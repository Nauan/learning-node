/**
 * Created by HOME on 15/06/2014.
 */

module.exports = function(app) {
    var passport = require('passport');
    var HomeController = {
        index: function(req, res) {
            res.render('home/index');
        },
        login: passport.authenticate('local', {
            successRedirect : '/contatos',   // redirect to the secure profile section
            failureRedirect : '/'}),         // redirect back to the signup page if there is an error}), '/contatos');,
        logout: function(req, res) {
            req.logout();
            res.redirect('/');
        }
    };
    return HomeController;
};