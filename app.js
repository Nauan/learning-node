var path = require('path');
var express = require('express');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var bodyParser = require('body-parser')
var methodOverride = require('method-override');
var passport = require('passport');
var LocalStrategy   = require('passport-local').Strategy;
var io = require('socket.io')
var passportIo = require('passport.socketio');
var load = require('express-load');
var mongoose = require('mongoose');
var error = require('./middleware/error');


const KEY = 'ntalk.sid';
const SECRET = 'ntalk.s3cr3t';
const MEMORY_STORE = new expressSession.MemoryStore();
var app = express();
global.db = mongoose.connect('mongodb://localhost/ntalk');


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cookieParser(SECRET));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(methodOverride(function(req, res){
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        var method = req.body._method
        delete req.body._method
        return method
    }
}));
app.use(expressSession({
    key:    KEY,
    store:  MEMORY_STORE,
    secret: SECRET
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());


passport.serializeUser(function(user, done) {
    done(null, user); //TODO Estou guardando o usuário inteiro na session store mesmo :D
});
passport.deserializeUser(function(obj, done) {
    done(null, obj);
});

load('models')
    .then('controllers')
    .then('routes')
    .into(app);

passport.use('local', new LocalStrategy(
    {passReqToCallback: true,
        usernameField : 'usuario[email]',
        passwordField : 'usuario[nome]'}, //Não estamos utilizando senha, porém o LocalStrategy obriga a ter senha :(
    function(req, email, password, done) {

        var UsuarioRepository = app.models.usuario; //Está pegando o módulo usuario da pasta models, através do express-load
        var query = {email: req.body.usuario.email};

        UsuarioRepository.findOne(query)
            .select('nome email contatos')
            .exec(function(erro, usuario) {
                if (usuario) {
                    return done(null, usuario);
                } else {
                    UsuarioRepository.create(req.body.usuario, function(erro, usuario) {
                        if (erro) {
                            return done(null, false, { message: 'Falha na autenticação: ' + erro });
                        } else {
                            return done(null, usuario);
                        }
                    });
                }
            });
    }
));

app.use(error.notFound);
app.use(error.serverError);


var server = app.listen(3000, function(){
    console.log("Ntalk no ar.");
});

io = io.listen(server);

io.use(passportIo.authorize({
    cookieParser: cookieParser,
    key:          KEY,               // the name of the cookie where express/connect stores its session_id
    secret:       SECRET,            // the session_secret to parse the cookie
    store:        MEMORY_STORE,      // we NEED to use a sessionstore. no memorystore please    >:D
    success:     onAuthorizeSuccess, // *optional* callback on success - read more below
    fail:        onAuthorizeFail     // *optional* callback on fail/error - read more below
}));

load('sockets')
    .into(io);



function onAuthorizeSuccess(data, accept){
    console.log('authorized connection to socket.io');
    accept(null, true);
}

function onAuthorizeFail(data, message, error, accept){
    if(error)
        throw new Error(message);
    console.log('failed to authorize connection to socket.io:', message);
    accept(null, false);
}