/**
 * Created by HOME on 17/06/2014.
 */
exports.notFound = function(req, res, next) {
    res.status(404);
    res.render('not-found');
};
exports.serverError = function(error, req, res, next) {
    res.status(500);
    res.render('server-error', {error: error});
};