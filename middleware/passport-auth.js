/**
 * Created by HOME on 22/06/2014.
 */
module.exports = function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/');
}