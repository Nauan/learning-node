/**
 * Created by HOME on 16/06/2014.
 */

//TODO Ver melhor maneira de criar esse filtro. No livro tem que adicionar ele como callback de cada rota. Será que tem que fazer isso mesmo?
module.exports = function(req, res, next) {
    if(!req.session.usuario) {
        return res.redirect('/');
    }
    return next();
};