var express = require('express');
var router = express.Router();

router.all('*', function(req, res, next) {
    console.log(req.url);
    if (req.url != '/' && !req.session.usuario) {
        return res.redirect('/');
    }
    next();
});

module.exports = router;
